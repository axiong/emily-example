// stdlib functionality
#include <iostream>

// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>

// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"

int main() {
  std::cout << "Anni made changes to the number of events to process"<<std::endl;
  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  event.readFrom( iFile.get() );

  //Declare and Define histograms
  TH1D *h_njets = new TH1D("h_njets", "h_njets", 20, 0, 20);
  TH1D *h_mjj = new TH1D("h_mjj", "h_mjj", 100, 0, 500);

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  
  //const Long64_t numEntries = event.getEntries();
  const Long64_t numEntries = 200; //changed to process fewer events

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {  

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    //std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    //Make accessible vector of jets
    std::vector<xAOD::Jet> vjets;

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      // print the kinematics of each jet in the event
      //std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;
      vjets.push_back(*jet);
    }

    h_njets->Fill( vjets.size());
    
    if( vjets.size() >= 2)
      h_mjj->Fill( (vjets.at(0).p4() + vjets.at(1).p4()).M() / 1000);

    // counter for the number of events analyzed thus far
    count += 1;
  }

  //open output file
  TFile *oFile = new TFile("Plots.root", "RECREATE");

  //Write Plot to file
  h_njets->Write();
  h_mjj->Write();

  TCanvas *c1 = new TCanvas("njets", "njets", 800, 600);
  h_njets->Draw();
  TCanvas *c2 = new TCanvas("mjj", "mjj", 800, 600);
  h_mjj->Draw();

  c1->SaveAs("njets.pdf");
  c2->SaveAs("mjj.pdf");

  //Close output file
  oFile->Close();

  // exit from the main function cleanly
  return 0;
}
